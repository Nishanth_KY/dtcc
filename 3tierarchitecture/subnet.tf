resource "aws_subnet" "public_az1" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.public_subnet_cidr_block_az1
  availability_zone = "${var.region}a"
  map_public_ip_on_launch = true
  tags = {
    Name = "dtcc-public_az1"
  }
}

resource "aws_subnet" "public_az2" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.public_subnet_cidr_block_az2
  availability_zone = "${var.region}b"
  map_public_ip_on_launch = true
  tags = {
    Name = "dtcc-public_az2"
  }
}

resource "aws_subnet" "private_az1" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.private_subnet_cidr_block_az1
  availability_zone = "${var.region}a"
  tags = {
    Name = "dtcc-private_az1"
  }
}

resource "aws_subnet" "private_az2" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.private_subnet_cidr_block_az2
  availability_zone = "${var.region}b"
  tags = {
    Name = "dtcc-private_az2"
  }
}

resource "aws_subnet" "db_private_az1" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.db_subnet_cidr_block_az1
  availability_zone = "${var.region}a"
  tags = {
    Name = "dtcc-db-private_az1"
  }
}

resource "aws_subnet" "db_private_az2" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.db_subnet_cidr_block_az2
  availability_zone = "${var.region}b"
  tags = {
    Name = "dtcc-db-private_az2"
  }
}
