resource "aws_instance" "web_server" {
  ami           = var.ami_id
  instance_type = var.web_server_instance_type
  subnet_id     = aws_subnet.public_az1.id
  key_name      = var.key_name
  vpc_security_group_ids = [aws_security_group.web_sg.id]

  user_data = <<-EOF
        #!/bin/bash
        sudo yum install nginx -y
        echo "<h2>Nginx is working in az1</h2>" | sudo tee /usr/share/nginx/html/az_info.html
        sudo systemctl restart nginx
        EOF


  tags = {
    Name = "Dtcc-Web-Server"
  }
}

resource "aws_instance" "app_server" {
  ami           = var.ami_id
  instance_type = var.app_server_instance_type
  subnet_id     = aws_subnet.private_az1.id
  key_name      = var.key_name
  vpc_security_group_ids = [aws_security_group.app_sg.id]

  tags = {
    Name = "Dtcc-App-Server"
  }
}

resource "aws_instance" "db_server_az1" {
  ami                     = var.ami_id
  instance_type           = var.db_server_instance_type
  subnet_id               = aws_subnet.db_private_az1.id
  key_name                = var.key_name
  vpc_security_group_ids  = [aws_security_group.db_sg.id]
  tags = {
    Name = "Dtcc-DB-Server-AZ1"
  }
}

resource "aws_instance" "web_server_az2" {
  ami           = var.ami_id
  instance_type = var.web_server_instance_type
  subnet_id     = aws_subnet.public_az2.id
  key_name      = var.key_name
  vpc_security_group_ids = [aws_security_group.web_sg.id]

  user_data = <<-EOF
        #!/bin/bash
        sudo yum install nginx -y
        echo "<h2>Nginx is working in az2</h2>" | sudo tee /usr/share/nginx/html/az_info.html
        sudo systemctl restart nginx
        EOF


  tags = {
    Name = "Dtcc-Web-Server-AZ2"
  }
}

resource "aws_instance" "app_server_az2" {
  ami           = var.ami_id
  instance_type = var.app_server_instance_type
  subnet_id     = aws_subnet.private_az2.id
  key_name      = var.key_name
  vpc_security_group_ids = [aws_security_group.app_sg.id]

  tags = {
    Name = "Dtcc-App-Server-AZ2"
  }
}
