provider "aws" {
  region = "ap-south-1"  
}

# AWS Vpc
# resource "aws_vpc" "testing" {
#   cidr_block = "10.0.0.0/16"
  
#   enable_dns_hostnames        = true
#   enable_dns_support          = true
#   enable_network_address_usage_metrics = false
  
#   tags = {
#     Name = "testing"
#   }
# }

# AWS Subnet
# resource "aws_subnet" "testing-subnet-public1" {
#   vpc_id               = "vpc-01ff1bc7aeebf8cea"
#   cidr_block           = "10.0.0.0/20"
#   availability_zone    = "ap-south-1a"
#   map_public_ip_on_launch = true
#   tags = {
#     Name = "testing-subnet-public1"
#   }
# }

# resource "aws_subnet" "testing-subnet-public2" {
#   availability_zone = "ap-south-1b"
#   cidr_block        = "10.0.16.0/20"
#   vpc_id            = "vpc-01ff1bc7aeebf8cea"
#   map_public_ip_on_launch = true
#   tags = {
#     Name = "testing-subnet-public2"
#   }
# }

# # AWS Internet Gateway
# resource "aws_internet_gateway" "testing_igw" {
#   vpc_id = "vpc-01ff1bc7aeebf8cea"

#   tags = {
#     Name = "testing-igw"
#   }
# }

# # AWS Route Table
# resource "aws_route_table" "testing_rtb_public" {
#   vpc_id = "vpc-01ff1bc7aeebf8cea"

#   tags = {
#     Name = "testing-rtb-public"
#   }

#   route {
#     cidr_block = "0.0.0.0/0"
#     gateway_id = aws_internet_gateway.testing_igw.id
#   }
# }

# # AWS Route Table Association
# resource "aws_route_table_association" "testing_rtb_public1" {
#   subnet_id        = "subnet-0c4434d00cb37ef16"
#   route_table_id   = "rtb-09f1c0746671ae2de"
# }

# resource "aws_route_table_association" "testing_rtb_public2" {
#   subnet_id        = "subnet-0727eec4777fbd831"
#   route_table_id   = "rtb-09f1c0746671ae2de"
# }

# # AWS Security Group
# resource "aws_security_group" "testing-sg" {
#   name        = "testing-sg"
#   vpc_id      = "vpc-01ff1bc7aeebf8cea"

#   // Ingress rule
#   ingress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   // Egress rule
#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
# }

# AWS Instances
resource "aws_instance" "test1" {
  ami                    = "ami-007020fd9c84e18c7"
  instance_type          = "t2.micro"
  subnet_id              = "subnet-0c4434d00cb37ef16"
  key_name               = "DTCC_keypair"
  associate_public_ip_address = true

  tags = {
    Name = "test1"
  }

  user_data = file("script.sh")
}

resource "aws_instance" "test2" {
  ami                    = "ami-007020fd9c84e18c7"
  instance_type          = "t2.micro"
  subnet_id              = "subnet-0727eec4777fbd831"
  key_name               = "DTCC_keypair"
  associate_public_ip_address = true

  tags = {
    Name = "test2"
  }

    user_data = file("script.sh")
}

# Load Balancer
resource "aws_lb" "test" {
  name                               = "test"
  internal                           = false
  load_balancer_type                 = "application"
  security_groups                    = ["sg-08a20426e773d3729"]
  subnets                            = ["subnet-0c4434d00cb37ef16", "subnet-0727eec4777fbd831"]
  enable_deletion_protection         = false
  enable_http2                       = true
  enable_cross_zone_load_balancing   = true
}

# Load Balancer Listener
resource "aws_lb_listener" "testing" {
  load_balancer_arn = aws_lb.test.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "forward"

    forward {
      target_group {
        arn    = aws_lb_target_group.target.arn
        weight = 1
      }

      stickiness {
        duration = 86400 
        enabled  = false
      }
    }
  }
}

# Target Group
resource "aws_lb_target_group" "target" {
  name                = "target"
  port                = 80
  protocol            = "HTTP"
  vpc_id              = "vpc-01ff1bc7aeebf8cea"
  deregistration_delay = 300
  target_type         = "instance"
  health_check {
    enabled             = true
    interval            = 30
    port                = "traffic-port"
    protocol            = "HTTP"
    path                = "/login"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
    matcher             = "200"
  }
}

# Target Group Attachment
resource "aws_lb_target_group_attachment" "testing" {
  for_each = {
    for idx, instance in { "test1" = aws_instance.test1, "test2" = aws_instance.test2 } :
    idx => instance
  }

  target_group_arn = aws_lb_target_group.target.arn
  target_id        = each.value.id
  port             = 8080
}
